'use strict';

var models = require('../models');

function findDistinctProductCategories() {
  return models.Product.findAll({
    attributes: ['category'],
    group: ['category'],
    include: []
  });
}

function findProduct(productId) {
  return models.Product.findOne({
    where: {
      id: productId
    },
    include: [
      models.Picture
    ]
  });
}

function findProductWithPrice(productId) {
  return new Promise((resolve, reject) => {
    var product = null;
    findProduct(productId)
      .then(aProduct => {
        product = aProduct;
        return findLatestProductPrice(product.id, 'pccomponentes');
      })
      .then(price => {
        product.latestPrice = price;
        resolve(product);
      })
      .catch(err => {
        reject(err);
      });
  });
}

function findProductPrices(productId) {
  return models.Price.findAll({
    where: {
      productId: productId
    },
    order: [
      ['createdAt', 'ASC'] //old prices first
    ]
  });
}

function findLatestProductPrice(productId, store) {
  store = store || 'pccomponentes';
  return models.Price.findOne({
    where: {
      productId: productId,
      store: store
    },
    order: [
      ['createdAt', 'DESC'] //recent prices first and we get the first one
    ]
  });
}

function findLatestPriceAndAddToList(product, productsList) {
  if (!productsList) productsList = [];

  return new Promise((resolve, reject) => {
    findLatestProductPrice(product.id, 'pccomponentes')
      .then(price => {
        product.latestPrice = price;
        productsList.push(product);
        resolve(productsList);
      })
      .catch(err => {
        reject(err);
      });
  });
}

function getProductsWithLatestPrice(products) {
  var p = Promise.resolve();

  products.forEach(function(product) {
    p = p.then(function(productsWithPrice) {
      return findLatestPriceAndAddToList(product, productsWithPrice);
    });
  });

  return p;
}

function findProductsInCategory(categoryId) {
  return models.Product.findAll({
    where: {
      category: categoryId
    },
    order: [
      ['createdAt', 'DESC'] //new products first
    ],
    include: [
      models.Picture
    ]
  });
}

function findProductsWithPriceInCategory(categoryId) {
  return new Promise((resolve, reject) => {
    findProductsInCategory(categoryId)
      .then(products => {
        return getProductsWithLatestPrice(products);
      })
      .then(productsWithPrice => {
        resolve(productsWithPrice);
      })
      .catch(err => {
        reject(err);
      });
  });
}

module.exports = {
  findDistinctProductCategories: findDistinctProductCategories,
  findProduct: findProduct,
  findProductWithPrice: findProductWithPrice,
  findProductPrices: findProductPrices,
  findProductsWithPriceInCategory: findProductsWithPriceInCategory
};
