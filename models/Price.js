'use strict';

module.exports = (sequelize, DataTypes) => {
  var Price = sequelize.define('Price', {

    productId: DataTypes.INTEGER,

    store: DataTypes.STRING,

    price: {
      type: DataTypes.DECIMAL
    },

    priceWithTaxes: {
      type: DataTypes.DECIMAL
    },

    stock: DataTypes.BOOLEAN,

    date: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    }

  });

  return Price;
};
